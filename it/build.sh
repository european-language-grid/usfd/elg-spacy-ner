#!/bin/bash

MAINTAG=$1
if [ -z "$MAINTAG" ]; then
  echo "Please specify the base image registry/repository:tag"
  exit 1
fi

shift

MYDIR=$( cd `dirname "$0"` && basename $PWD )
MAINTAG_TAIL=${MAINTAG#*:}
if [ "$MAINTAG_TAIL" = "$MAINTAG" ]; then
  # no tag - assume latest
  MAINTAG_TAIL=latest
fi
THISLANGTAG=${MAINTAG%/*}/$MYDIR:${MAINTAG_TAIL}

echo Main tag is $MAINTAG
echo Sub tag is $THISLANGTAG

if [ -z "$*" ]; then
  BUILDX_EXTRA_ARGS="--load"
fi

cat Dockerfile.template | sed "s^@MAINTAG@^$MAINTAG^" | docker buildx build $BUILDX_EXTRA_ARGS "$@" -t $THISLANGTAG -
