spaCy NER for ELG
=================

Simple wrapper for the spaCy named entity recogniser.  Accepts plain text input (either directly POSTed `text/plain` or an ELG `text` request JSON message) and returns ELG `annotations` response with the spaCy named entity annotation types for the relevant model.  This is implemented as a base image with the Python code and logic, and then a child image derived from that for each language model.  The image is based on quart and hypercorn, so can handle HTTP/2 requests (clear text with prior knowledge) as well as HTTP/1.1.  It is also possible to inject a key and certificate to have the service run in https mode, in which case it will negotiate HTTP 1.1 or 2 using ALPN in the normal way.

Building
--------

Build the base image first, by running

```
./build.sh [image-tag]
```

in the `base` directory.  The image tag defaults to `spacy_ner/base:latest` if omitted.  Once the base image is built, you can build the per-model child images by running the same command in each directory in turn

```
./build.sh [base-image-tag] [child-image-tag]
```

As before, the base image tag defaults to `spacy_ner/base:latest` if omitted, and the child image tag is derived from the base image tag by replacing `base` with the appropriate model directory name.

Running
-------

By default a container running from one of the child images runs a single hypercorn worker, but this can be overridden by setting the `WORKERS` environment variable in the container (e.g. using the `-e WORKERS=3` option to `docker run`).  The service listens on port 8000, and accepts POST requests at `http://<container-ip>:8000/process`.

Any additional parameters passed to `docker run` will be passed through to the `hypercorn` command, so for example to configure HTTPS you could use:

```
docker run -v ./certs:/certs -p 8000:8000 spacy_ner/en:latest --cert-file=/certs/cert.pem --key-file=/certs/key.pem
```
