#!/bin/bash

TAG=$1
shift

if [ -z "$TAG" ]; then
  TAG=spacy_ner/base:latest
fi

if [ -z "$*" ]; then
  BUILDX_EXTRA_ARGS="--load"
fi

exec docker buildx build $BUILDX_EXTRA_ARGS "$@" -t $TAG .
